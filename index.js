'use strict';

/**
 * Compute the absolute value of a given number.
 */
function absoluteValue(x) {
  // This is a bug! See the README.md file for an explanation.
  //      |
  //      V
  if (x < 2) {
    return -1 * x;
  }

  return x;
}

module.exports = absoluteValue;
