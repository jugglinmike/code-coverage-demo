# Code coverage: necessary, but not sufficient

Software developers write automated tests to prove that their code works as
expected (the good ones do, anyway!). For large projects, it can be difficult
to be sure that the tests are actually using every single line of code. To help
make sure they haven't "missed a spot," devs often use sophisticated code
coverage analysis tools. Those tools are able to flag lines of code which
aren't used by a test suite.

This project demonstrates how achieving 100% code coverage does not mean the
code is error-free.

## Demonstration

The file named `index.js` implements one small JavaScript function,
`absoluteValue`, copied here for clarity:

```js
/**
 * Compute the absolute value of a given number.
 */
function absoluteValue(x) {
  if (x < 2) {
    return -1 * x;
  }

  return x;
}
```

The file named `test.js` defines the automated tests:

```js
suite('absoluteValue', () => {
  test('positive number', () => {
    assert.equal(absoluteValue(3), 3);
  });

  test('negative number', () => {
    assert.equal(absoluteValue(-8), 8);
  });
});
```

We can run the tests from the command line to verify that `absoluteValue`
behaves how we expect:

    $ npm test
    
    > code-coverage-demo@1.0.0 test
    > mocha --ui tdd test.js
    
    
      absoluteValue
        ✔ positive number
        ✔ negative number
    
    
      2 passing (4ms)

There's just one problem: `absoluteValue` has a bug!

If we call `absoluteValue` with value `1` (positive one), then it will return
`-1` (negative one). Negative one is **not** the absolute value of positive
one. The `if` condition really ought to be checking `x < 0`.

This bug is interesting because it "sneaks by" the project's tests.

We might expect this if the tests didn't use (or "cover") every line of the
function. We wouldn't be so surprised to learn that *untested* code had a bug.
That's not what's going on here, though. When the tests run, every line of code
is covered.

You can convince yourself of this by carefully reading the tests and "stepping"
through the function's code, one line at a time. A much faster way to do that
is to use a code coverage analysis tool. This will programmatically verify that
the tests really do use every single line of the function:

    $ npm run coverage
    
    > code-coverage-demo@1.0.0 coverage
    > nyc npm test
    
    
    > code-coverage-demo@1.0.0 test
    > mocha --ui tdd test.js
    
    
      absoluteValue
        ✔ positive number
        ✔ negative number
    
    
      2 passing (5ms)
    
    ----------|---------|----------|---------|---------|-------------------
    File      | % Stmts | % Branch | % Funcs | % Lines | Uncovered Line #s 
    ----------|---------|----------|---------|---------|-------------------
    All files |     100 |      100 |     100 |     100 |                   
     index.js |     100 |      100 |     100 |     100 |                   
    ----------|---------|----------|---------|---------|-------------------

How can it be that the tests use the buggy code, but they pass, anyway?

This is possible because the incorrect line of code (that is: the condition in
the `if` statement) only occasionally expresses incorrect behavior. We wrote a
test to "cover" that line, and the test even passes, but that doesn't prove
that the line will behave correctly in all cases.

The moral is that even if we have a passing test suite and perfect test
coverage, we can still have bugs in our code. Unit tests and code coverage
tools are a great help in writing software, but they can't guarantee that our
code is bug-free. When it comes to writing correct code, there's just no
substitute for careful programming and peer review!

## See for yourself

You can run the experiment yourself by following these steps:

1. Install [Node.js](https://nodejs.org/)
2. Download the code in this repository
3. Install the project's Node.js dependencies by running the following command in a terminal:

       $ npm install

## Tools

This project uses a couple free and open source software tools. These are
widely-used, fully-featured pieces of software, so they're great options for
use in your own projects.

- [Mocha](https://mochajs.org/)) - a testing framework for JavaScript
- [nyc](https://www.npmjs.com/package/nyc) - a JavaScript code coverage analyzer

## License

Copyright 2022 Mike Pennisi under [the GNU General Public License
v3.0](https://www.gnu.org/licenses/gpl-3.0.html)
