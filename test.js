'use strict';
const assert = require('assert');
const absoluteValue = require('./index');

suite('absoluteValue', () => {
  test('positive number', () => {
    assert.equal(absoluteValue(3), 3);
  });

  test('negative number', () => {
    assert.equal(absoluteValue(-8), 8);
  });
});
